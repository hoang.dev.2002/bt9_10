function NhanVien(account,fullName,email,password,date,basicSalary,position,workingHours){
    this.taiKhoan=account,
    this.hoTen=fullName,
    this.email=email,
    this.matKhau=password,
    this.ngayLam=date,
    this.luongCoBan=basicSalary,
    this.chucVi=position,
    this.soGioLam=workingHours,
    this.tongLuong=function(){
        var total;
        if(this.chucVi=="Sếp"){
            total=this.luongCoBan*3;
        }else if(this.chucVi=="Trưởng phòng"){
            total=this.luongCoBan*2;
        }else{
            total=this.luongCoBan;
        }
        return total ;
}
this.xepLoai=function(){
    var rank="";
    if(this.soGioLam>=192){
    rank="Nhân viên xuất sắc";
    }else if(this.soGioLam>=176 ){
        rank="Nhân viên giỏi"
    }else if(this.soGioLam>=160){
     rank="Nhân viên khá" 
    }else{
        rank="Nhân viên trung bình"
    }
    return rank;

}
}